from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    test_data = {
        'name': 'john',
        'email': 'john@gmail.com'
    }
    request.graylog.info("Rendered homepage {test_data}", test_data=test_data)
    return HttpResponse('good')
