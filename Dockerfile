FROM python:3.8

ENV APP_HOME=/usr/src/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/public
WORKDIR $APP_HOME

RUN pip install --upgrade pip
COPY requirements.txt ./
# RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -r requirements.txt

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . .
